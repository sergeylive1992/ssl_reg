import random
import requests
from multiprocessing import Pool

import time


def create_char_id(x):
    ids = []
    chars = 'qwereyuiopasdfghjklzxvcbnm'
    for i in range(0, x):
        r_1 = random.randint(0, len(chars) - 1)
        r_2 = random.randint(0, len(chars) - 1)
        ud1 = chars[:r_1] + chars[:r_2]
        ud2 = chars[:r_2] + chars[:r_1]
        u = [ud1, ud2]
        ids.append(u)
    return ids


def create_id(x):
    ids = []
    for i in range(0, x):
        r_1 = random.randint(10002, 5000000)
        ids.append(r_1)

    print(ids)
    return ids


def check_response(x):

    # share
    link = 'http://genistats.com:52101/share-lnk'

    # direct messages
    # link = 'http://supersonics.ru/lnk?o=' + str(x) + '&f=' + str(x + 1)

    r = requests.get(link)
    print(r.content)
    print(link)
    return r.content


if __name__ == '__main__':
    tim = time.time()
    y = create_id(500)
    pool = Pool(500)
    pool.map(check_response, y)
    pool.close()
    pool.join()
    tim2 = time.time()
    print('Execution time ---------> ' + str(round(tim2 - tim)) + 's')