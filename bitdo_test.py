import time

import os
from selenium import webdriver
import threading


def short_link(step_nth):
    driver_step_nth = webdriver.Chrome(os.getcwd() + '/' + 'chromedriver')

    file = open('tumblrs_new.txt', 'r')
    file_lines = file.readlines()

    for line in file_lines[0::step_nth]:

        driver_step_nth.get('https://bit.do/')
        driver_step_nth.find_element_by_id('url').send_keys(line.split('\n')[0])

        time.sleep(1)

        driver_step_nth.find_element_by_xpath('/html/body/div[4]/table/tbody/tr/td[2]/form/table/tbody/tr[4]/td[2]/input').click()

        time.sleep(1.5)

        link = driver_step_nth.find_element_by_xpath('//*[@id="url_shortened_stats"]').text
        link_https = link.replace('http', 'https')

        print(link_https.split('-')[0])

    driver_step_nth.close()
    driver_step_nth.quit()

    file.close()


t1 = threading.Thread(target=short_link, args=(1,))
t2 = threading.Thread(target=short_link, args=(2,))

t1.start()
t2.start()
