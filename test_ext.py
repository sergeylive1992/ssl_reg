import requests

extensions = [["https://chrome.google.com/webstore/detail/open-tabs/jgbplljflpafcagbdpnfoadempkjeobo", "Open Tabs"],
              ["https://chrome.google.com/webstore/detail/readlater/lcagnphjfhnmhooimflpocjcemgeogbi", "Read_Later"],
              ["https://chrome.google.com/webstore/detail/lazy-scroll/ekigohdbdhmfcfnioggnnklmjhdofaba", "Lazy Scroll"],
              ["https://chrome.google.com/webstore/detail/scroll-to/gnnopnbpjmcdbcmejcgaoljcfdadlnha", "ScrollTo"],
              ["https://chrome.google.com/webstore/detail/%D0%BF%D0%BE%D0%B8%D1%81%D0%BA-%D0%BD%D0%B0-kinosklad/nmofnlljincdddlklgebejmpfliidpof", "Поиск на Kinosklad"],
              ["https://chrome.google.com/webstore/detail//aaejabooilfjaacohleofkajajjgmkpk", "Фильмы в 720HD"],
              ["https://chrome.google.com/webstore/detail/goodlayer/fnohdcofeiglkedmbecaddfojjiiglbh?hl=ru", "GoodLayer"],
              ["https://chrome.google.com/webstore/detail/blacklist-of-sites/dlnbejhkimoglmeboflnjocklpejicdg", "Blacklist Of Sites"],
              ["https://chrome.google.com/webstore/detail/user-agent-switcher/ehdgmoiahikjilhdgjkhakddncffblko", "User-agent-switcher"],
              ["https://chrome.google.com/webstore/detail/icgnikmoipkihbgpcpkalhoiaoecgadb", "dark-mode"],
              ["https://chrome.google.com/webstore/detail/css3-generator/dbagjdaijciimblbbkhckooefkdjkbkc", "CSS3 Generator"],
              ["https://chrome.google.com/webstore/detail/save-image-as-type/chjinpgdnoclpojmmamlfefadeemcoci", "Save Image As Type"],
              ["https://chrome.google.com/webstore/detail/new-tab-on-right-click/mkehfjofokpbokcillmdcnjmdoejkljb", "New tab on right click"],
              ["https://chrome.google.com/webstore/detail/mbcphegdommpacababbgoilfgjoadomp", "Automatic page scrolling"],
              ["https://chrome.google.com/webstore/detail/lcjgldekopeocahlabagdnppcdgelfmk", "Background eclipse"],
              ["https://chrome.google.com/webstore/detail/cjdokmpgeiomnjniieolghokidfbohck", "Easy Search"],
              ["https://chrome.google.com/webstore/detail/faodgkalinlhlchclobkkgboldhmedja", "Nightmode of the page"],
              ["https://chrome.google.com/webstore/detail/page-dimming/mioafphdlblccoondkgnpkhjknddidjp", "Page dimming"]]

for url in extensions:
    r = requests.get(url[0])
    print(url[1]  + '   ' + str(r.status_code))