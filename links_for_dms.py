import random

links = ['meldere.ru','qwenom.ru','marenelli.ru','lukarelli.ru','indzagi.ru','shewanton.ru','welbeck.ru','berchamp.ru','nestaff.ru','yorck.ru','riwaldo.ru','geneple.ru','kelde.ru','fushulde.ru','mulide.ru','geldeere.ru','geraldey.ru','mirecont.ru','kulibala.ru','kartofas.ru','notedot.ru','hokoland.ru','faderley.ru','derevon.ru','cherelof.ru','doolims.ru','fagorey.ru','fotorot.ru','limajo.ru','joblen.ru']
trusted_links = ['https://youtube.com','https://facebook.com','https://wikipedia.com','https://amazon.com','https://twitter.com','https://lenta.ru','https://yandex.ru','https://ebay.com','https://aliexpress.com','https://iz.ru','https://gazeta.ru','https://ura.news','https://regnum.ru','https://ura.news','https://dni.ru','https://1tv.ru','https://cosmo.ru','https://rutube.ru','https://kommersant.ru','https://smi2.ru','https://megogo.net','https://news.bigmir.net','https://www.bbc.co.uk','https://rbc.ru','https://avito.ru','https://drom.ru','https://www.w3schools.com','https://rutube.ru','https://woman.ru','https://www.kp.ru','http://www.rosbalt.ru','https://topwar.ru','https://www.novayagazeta.ru','https://auto.ria.com','https://forbes.ru','https://gismeteo.ru','https://www.blablacar.com','https://motor.ru','https://www.baby.ru','https://news.yandex.ru','https://lady.mail.ru','https://hsdigital.ru','http://www.hearst-shkulev-media.ru','http://auto.ru','http://www.mvideo.ru','http://banki.ru','https://pogoda.yandex.ru','https://disk.yandex.ru','https://moevideo.biz','https://russian.alibaba.com','https://market.yandex.ru','https://live.com','https://aol.com','https://myspace.com','https://craigslist.org']

links_count = 5000

for link in links:
    rand1 = random.randint(0, len(trusted_links))

    print('http://' + link +', ' + trusted_links[rand1] + ', ' + str(links_count))