import os

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

driver = webdriver.Chrome(os.path.join('chromedrivers', 'chromedriver.exe'))
driver.implicitly_wait(1)

categories = os.listdir('store_emails')

for i, category in enumerate(categories):
    file = open('test_' + str(i) + '.txt', mode='w+', encoding='UTF-8')
    with open(os.path.join('store_emails', category)) as f:
        lines = f.readlines()

    i = 1
    emails = []
    for link in lines:
        try:
            driver.get(link)
            email = driver.find_element_by_class_name('C-b-p-rc-D-R').get_attribute('href')
            if 'mail' in email:
                if email not in emails:
                    print(email)
                    emails.append(email)
        except NoSuchElementException or ConnectionError:
            pass

        i = i + 1

    file.write(str(emails))
    file.close()
driver.close()
driver.quit()
