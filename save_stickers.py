import os
import PIL
import requests
from PIL import Image
from bs4 import BeautifulSoup


def resize_imgs_in_folder(imgs_path):
    for root, dirs, files in os.walk(imgs_path):
        for img in files:
            if img.endswith('.png'):
                imgg = Image.open(root + '/' + img)
                imgg = imgg.resize((250, 250), PIL.Image.ANTIALIAS)
                imgg.save(root + '/' + img)


def save_pack(packs):
    for pack in packs:
        link = 'https://tlgrm.ru/stickers/' + pack[0]
        name = link.split('/')[-1].lower()
        r = requests.get(link)
        bs = r.content
        soup = BeautifulSoup(bs, 'lxml')
        div = soup.find('img', {'class': 'b-sticker__image js-lazy'})
        href = div.attrs['data-original'].split('192/1.png')[:-1][0]

        if not os.path.exists('sticker_packs/' + name + '/'):
            os.makedirs('sticker_packs/' + name + '/')

        for idx in range(1, 70):
            l = href + str(idx) + '.png'
            r2 = requests.get(l)

            if r2.status_code == 200:
                print(str(idx))
                with open('sticker_packs/' + name + '/' + str(idx) + '.png', 'wb') as f:
                    f.write(r2.content)
            else:
                break

        pack_info = open('sticker_packs/' + name + '/info.json', 'w+')
        pack_info.write('{"name":"%s", "length":%d}' % (pack[1], idx - 1))

        resize_imgs_in_folder(os.path.join('sticker_packs/' + name))


pack_list = [['StupidsDogs', 'Смешные собачки']]
save_pack(pack_list)