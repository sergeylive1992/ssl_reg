# m1-shop.ru adv.extland@extland.com  Na9DDZr5CaRrdfEsdvsdv
import time
from telnetlib import EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome()
driver.set_window_size(1500, 1500)
driver.implicitly_wait(10)


def scroll_to_bottom(pause_time):

    scroll_pause_time = pause_time

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")

    while True:
        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(scroll_pause_time)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height


# json
json_file_list = []

# credentials
email = 'adv.extland@extland.com'
password = 'Na9DDZr5CaRrdfEsdvsdv'

# login url
driver.get('http://m1-shop.ru/login')

# login
driver.find_element_by_xpath('//*[@id="tab0"]/div/div/div[2]/div/form/div[1]/div[2]/input').send_keys(email)
driver.find_element_by_xpath('//*[@id="tab0"]/div/div/div[2]/div/form/div[2]/div/input').send_keys(password)
driver.find_element_by_xpath('//*[@id="tab0"]/div/div/div[2]/div/form/button').click()

# offer link
offers_links_file = open('/home/ser/Desktop/offers.txt', 'r')
json_file_txt = open('offers_json_1.txt', 'w+')

offers_links = offers_links_file.readlines()


for link in offers_links:
    try:
        # empty dictionary
        json_file = {}

        # split offer link to get ID
        json_file['id'] = link.split('add/')[1]

        # get offer link from list
        driver.get(link)

        # find all elements first
        landing_rows = driver.find_elements_by_css_selector('.landing-row')

        offer_name = driver.find_element_by_xpath('//*[@id="form-save-link"]/div/div[1]/span[1]').text.split('- ')[1]

        # set offer name
        json_file['name'] = offer_name

        # get all locations
        driver.find_element_by_xpath('//*[@id="select2-domain_landing-container"]').click()
        location_2_list = driver.find_element_by_xpath('//*[@id="select2-domain_landing-results"]')
        location_2 = location_2_list.find_elements_by_tag_name('li')

        all_locations = []
        for loc in location_2:
            all_locations.append(loc.text)

        # check if prokl exist
        is_prokl = len(driver.find_elements_by_xpath("//*[@id='domain_proklWrap']/label"))

        print('Prokl is - ' + str(is_prokl))
        if is_prokl == 0:

            # sort by EPC
            for i in range(0, 2):
                driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/thead/tr/th[7]').click()

            # choose order
            for c in range(1, (len(landing_rows) - 1)):

                # find type of order
                type_of_offer = driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(c) + ']/td[4]/a/span').text
                print(type_of_offer)

                # check if order web or adaptive
                if 'Адаптив' in type_of_offer:

                    # select offer with highest EPC
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(c) + ']/td[1]').click()
                    print(c)
                    break

                if 'Веб' in type_of_offer:

                    # select offer with highest EPC
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(c) + ']/td[1]').click()
                    print(c)
                    break

                if 'Web' in type_of_offer:

                    # select offer with highest EPC
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(c) + ']/td[1]').click()
                    print(c)
                    break

                if 'Ленд' in type_of_offer:

                    # select offer with highest EPC
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(c) + ']/td[1]').click()
                    print(c)
                    break

                elif c == len(landing_rows) - 2:
                    print(c)
                    driver.find_element_by_xpath(
                        '//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]').click()
                    break



            # scroll to bottom
            scroll_to_bottom(0.8)

            # click on header
            driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[4]/div[1]/div/div[1]').click()

            # scroll to bottom
            scroll_to_bottom(0.8)

            time.sleep(2)

            # select on button about delivery
            driver.find_element_by_name('advance_delivery').click()

            # scroll to bottom
            scroll_to_bottom(0.8)

            time.sleep(1)

            # save offer
            driver.find_element_by_xpath('//*[@id="save-link"]').click()

            time.sleep(1)

            # get link
            offer_link = driver.find_element_by_xpath('//*[@id="result-url"]').get_attribute('value')
            print(offer_link)

            json_file['url'] = offer_link

            count = 1
            offers = []
            locations = []
            for landing in landing_rows[0:(len(landing_rows) - 2)]:

                offer = {}

                try:
                    location = landing.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(count) + ']/td[4]/a').get_attribute('href')

                except:
                    break

                if not 'frame' in location:
                    location = location.split('.ru')[1][:-1]

                    for locc in all_locations:
                        new_location = locc + location
                        locations.append(new_location)

                # offer['location'] = locationn

                offers.append(offer)

                count = count + 1

            json_file['locations'] = locations
            json_file_list.append(json_file)

        else:

            for i in range(0, 2):
                driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/thead/tr/th[7]').click()

            # scroll to bottom
            scroll_to_bottom(0.8)

            # choose order
            for c in range(1, (len(landing_rows) - 1)):

                # find type of order
                type_of_offer = driver.find_element_by_xpath(
                    '//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[4]/a/span').text

                print(type_of_offer)

                # check if order web or adaptive
                if 'Адаптив' in type_of_offer:
                    # select offer with highest EPC
                    time.sleep(1)
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()

                    print(c)
                    break

                if 'Веб' in type_of_offer:

                    # select offer with highest EPC
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()
                    print(c)
                    break

                if 'Web' in type_of_offer:

                    # select offer with highest EPC
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()
                    driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()
                    print(c)
                    break

                if 'Ленд' in type_of_offer:
                    # select offer with highest EPC
                    driver.find_element_by_xpath(
                        '//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()
                    driver.find_element_by_xpath(
                        '//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(c) + ']/td[2]').click()
                    print(c)
                    break


                elif c == len(landing_rows) - 2:
                    driver.find_element_by_xpath(
                        '//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[1]/td[2]').click()
                    break

            # driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[1]/td[2]').click()

            # scroll to bottom
            scroll_to_bottom(0.8)

            # click on header
            driver.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[4]/div[1]/div/div[1]').click()

            # scroll to bottom
            scroll_to_bottom(0.8)

            time.sleep(2)

            # select on button about delivery
            driver.find_element_by_name('advance_delivery').click()

            # scroll to bottom
            scroll_to_bottom(0.8)

            time.sleep(1)

            # save offer
            driver.find_element_by_xpath('//*[@id="save-link"]').click()

            time.sleep(2)

            # get link
            offer_link = driver.find_element_by_xpath('//*[@id="result-url"]').get_attribute('value')
            print(offer_link)

            json_file['url'] = offer_link

            driver.execute_script("window.scrollTo(0, 600)")

            count = 1
            offers = []
            locations = []
            for landing in landing_rows[0:(len(landing_rows) - 2)]:

                offer = {}
                try:
                    location = landing.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div[2]/div[3]/table/tbody/tr[' + str(count) + ']/td[4]/a').get_attribute('href')

                except:
                    break
                # epc = landing.find_element_by_xpath('//*[@id="form-generate"]/div[1]/div[2]/div/div[3]/table/tbody/tr[' + str(count) + ']/td[7]/span').text

                if not 'frame' in location:
                    location = location.split('.ru')[1][:-1]

                    for locc in all_locations:
                        new_location = locc + location
                        locations.append(new_location)


                # offer['location'] = locationn

                offers.append(offer)

                count = count + 1

            json_file['locations'] = locations
            json_file_list.append(json_file)
    except:
        print('Error on offer ' + link)


# write to file
json_file_txt.write(str(json_file_list))
offers_links_file.close()

driver.close()
driver.quit()
