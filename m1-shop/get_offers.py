# m1-shop.ru adv.extland@extland.com  Na9DDZr5CaRrdfEsdvsdv
import time
from telnetlib import EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome()
driver.set_window_size(1500, 1500)
driver.implicitly_wait(1)

email = 'adv.extland@extland.com'
password = 'Na9DDZr5CaRrdfEsdvsdv'

driver.get('http://m1-shop.ru/login')

driver.find_element_by_xpath('//*[@id="tab0"]/div/div/div[2]/div/form/div[1]/div[2]/input').send_keys(email)
driver.find_element_by_xpath('//*[@id="tab0"]/div/div/div[2]/div/form/div[2]/div/input').send_keys(password)
driver.find_element_by_xpath('//*[@id="tab0"]/div/div/div[2]/div/form/button').click()

driver.get('http://m1-shop.ru/offers/')

# driver.find_element_by_xpath('//*[@id="offers_filters"]/div[1]/div[3]/div[1]/span').click()

# driver.find_element_by_xpath('//*[@id="sel-geo_chzn"]/ul/li/input').send_keys('Литва' + Keys.ENTER)
# driver.find_element_by_xpath('//*[@id="bubble_geo"]/button').click()

time.sleep(3)


def scroll_to_bottom(pause_time):

    scroll_pause_time = pause_time

    # Get scroll height
    last_height = driver.execute_script("return document.body.scrollHeight")

    while True:
        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(scroll_pause_time)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height


# init scroll to bottom function
scroll_to_bottom(0.8)


count_offers = driver.find_elements_by_class_name('results')


# change country name to iso
def iso_country(elem):

    if 'Россия' in elem:
        elem = 'RU'
    if 'Украина' in elem:
        elem = 'UA'
    if 'Беларусь' in elem:
        elem = 'BY'
    if 'Казахстан' in elem:
        elem = 'KZ'
    if 'Киргизия' in elem:
        elem = 'KG'
    if 'Армения' in elem:
        elem = 'AM'
    if 'Молдова' in elem:
        elem = 'MD'
    if 'Грузия' in elem:
        elem = 'GE'
    if 'Польша' in elem:
        elem = 'PL'
    if 'Германия' in elem:
        elem = 'DE'
    if 'Великобритания' in elem:
        elem = 'GB'
    if 'Латвия' in elem:
        elem = 'LV'
    if 'Литва' in elem:
        elem = 'LT'
    if 'Нидерланды' in elem:
        elem = 'NL'
    if 'Эстония' in elem:
        elem = 'EE'
    if 'Болгария' in elem:
        elem = 'BG'
    if 'Румыния' in elem:
        elem = 'RO'
    if 'Италия' in elem:
        elem = 'IT'
    if 'Испания' in elem:
        elem = 'ES'
    if 'Австрия' in elem:
        elem = 'AT'
    if 'Греция' in elem:
        elem = 'GR'
    if 'Португалия' in elem:
        elem = 'PT'
    if 'Словакия' in elem:
        elem = 'SK'
    if 'Франция' in elem:
        elem = 'FR'
    if 'Чехия' in elem:
        elem = 'CZ'
    if 'Венгрия' in elem:
        elem = 'HU'
    if 'Финляндия' in elem:
        elem = 'FI'
    if 'Ирландия' in elem:
        elem = 'IE'
    if 'Кипр' in elem:
        elem = 'CY'
    if 'Бельгия' in elem:
        elem = 'BE'
    if 'Дания' in elem:
        elem = 'DK'
    if 'Люксембург' in elem:
        elem = 'LU'
    if 'Азербайджан' in elem:
        elem = 'AZ'

    countries.append(elem)


count = 1
for offer in count_offers:


    # of = len(offer.find_elements_by_xpath('/html/body/div[3]/div[5]/table/tbody/tr[' + str(count) + ']/td[2]/div[3]'))
    #
    # print(of)

    button_value = offer.find_element_by_xpath('/html/body/div[3]/div[6]/table/tbody/tr[' + str(count) + ']/td[8]/a').text

    # if of == 0:

    if 'добавить' in button_value :

        offer_name = offer.find_element_by_xpath('/html/body/div[3]/div[6]/table/tbody/tr[' + str(count) + ']/td[2]/div[1]/a').text
        offer_link = offer.find_element_by_xpath('/html/body/div[3]/div[6]/table/tbody/tr[' + str(count) + ']/td[2]/div[1]/a').get_attribute('href')
        offer_countries_list = offer.find_element_by_xpath('/html/body/div[3]/div[6]/table/tbody/tr[' + str(count) + ']/td[4]/ul')
        offer_countries = offer_countries_list.find_elements_by_tag_name('li')

        countries = []


        # try:
        #     offer_other_countries_list = offer.find_element_by_css_selector('span[id^="showGeos"]')
        #     offer_other_countries = offer_other_countries_list.find_elements_by_tag_name('li')
        #
        #     for li in offer_other_countries:
        #         country = li.text
        #         iso_country(country)
        #
        # except:
        #     print('No other countries')

        for li in offer_countries:

            country = li.text
            iso_country(country)

        print(','.join(countries))
        print('')

        print(offer_name)
        print(offer_link)
        print('')
        print('')
        print('')

    else:
        print('Offer is ready')

    # print(count)
    count = count + 1



driver.close()
driver.quit()

