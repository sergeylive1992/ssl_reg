import random
import urllib.request

import os
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


def save_imgs_from_ok(start_index, path_to_links):
    images_file = open(path_to_links)
    path_to_saved = 'saved_imgs/'
    path_where_to_save = 'imgs_original/'

    images = images_file.readlines()

    for idx, img in enumerate(images):
        response = urllib.request.urlopen(img)
        urll = str(response.geturl())

        if urll == str(img.split('\n')[0]):
            urllib.request.urlretrieve(img, "saved_imgs/" + str(idx) + ".png")
            print(str(idx) + ' is saved')

        else:
            print('No img')
            pass

    images_file.close()

    filenames = os.listdir(path_to_saved)

    for filename in filenames:
        os.rename(path_to_saved + '/' + filename, path_where_to_save + str(start_index) + '.png')
        start_index += 1


def img_gen(amount, start_index):
    for i in range(0, amount):

        words = ['ПОДАРКИ', 'СТИКЕРЫ', 'ОТКРЫТКИ']

        r1 = random.randint(0, 255)
        r2 = random.randint(0, 255)
        r3 = random.randint(0, 255)
        # r4 = random.randint(100, 135)
        r4 = random.randint(90, 105)
        r5 = random.randint(100, 245)
        r6 = random.randint(0, 75)
        r7 = random.randint(180, 230)
        # r8 = random.randint(480, 500)
        # r9 = random.randint(220, 250)

        r8 = random.randint(300, 330)
        r9 = random.randint(220, 240)

        base = Image.new('RGB', (r8, r9), (r7, r5, r6))

        # first_pic = Image.open('imgs_original/' + str(i) + '.png').convert('RGBA')
        # second_pic = Image.open('imgs_original/frame.png').convert('RGBA')
        # third_pic = Image.open('imgs_original/background.png').convert('RGBA')
        # first_pic = first_pic.resize((110, 110), Image.ANTIALIAS)
        # third_pic = third_pic.resize((180, 180), Image.ANTIALIAS)
        # second_pic = second_pic.resize((180, 180), Image.ANTIALIAS)
        arrow_pic = Image.open('imgs_original/arrow.png').convert('RGBA')

        fnt = ImageFont.truetype('fonts/BrushType_Bold.ttf', 54)
        fnt2 = ImageFont.truetype('fonts/BrushType_Bold.ttf', 55)
        fnt3 = ImageFont.truetype('fonts/BrushType_Bold.ttf', 30)

        # get a drawing context
        d = ImageDraw.Draw(base)

        # d.text((20, r4), "БЕСПЛАТНЫЕ", font=fnt2, fill=("#fff"))
        # d.text((44, (r4 + 40)), random.choice(words), font=fnt2, fill=("#fff"))
        # d.text((50, r4 - 45), "Смотри по ссылке", font=fnt3, fill=("#fff"))

        d.text((20, r4), "БЕСПЛАТНЫЕ", font=fnt2, fill=("#fff"))
        d.text((44, (r4 + 40)), random.choice(words), font=fnt2, fill=("#fff"))
        d.text((95, r4 + 85), "ДЛЯ ОК", font=fnt3, fill=("#fff"))
        d.text((50, r4 - 35), "Смотри по ссылке", font=fnt3, fill=("#fff"))


        # base.paste(third_pic, (293, 34), third_pic)
        # base.paste(second_pic, (293, 34), second_pic)
        # base.paste(first_pic, (327, 65), first_pic)

        base.paste(arrow_pic, (110, r4 - 90), arrow_pic)

        base.save('imgs_out/' + str(i + start_index) + '.png', 'PNG')


# save_imgs_from_ok(2500, 'ok_images_5.txt')
img_gen(amount=3000, start_index=0)





