from PIL import Image

base = Image.new('RGB', (500,500), (256, 256, 256))
rect_1 = Image.new('RGB', (100, 100), (0, 0, 0)).rotate(45)

base.paste(rect_1, (50, 50))
base.show()