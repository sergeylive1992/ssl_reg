import urllib.request
import os


path_to_saved = 'saved_imgs/'
path_where_to_save = 'images_ok/'


def get_img(paths):
    for path in paths:
        images_file = open(path)
        images = images_file.readlines()

        for idx, img in enumerate(images):
            response = urllib.request.urlopen(img)
            urll = str(response.geturl())

            if urll == str(img.split('\n')[0]):
                urllib.request.urlretrieve(img, 'saved_imgs/' + str(idx) + ".png")
                print(str(idx) + ' is saved')

            else:
                print('No img')
                pass

        images_file.close()


paths_img = ['img_ok\\ok_images.txt']


# get_img(paths_img)

filenames = os.listdir(path_to_saved)

i = 0
for filename in filenames:
    os.rename(os.path.join(path_to_saved, filename), os.path.join(path_where_to_save, str(i) + '.png'))
    i += 1
