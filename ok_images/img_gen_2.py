import random
import os

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import math


def img_gen(amount, start_index):

    number_of_pics = os.listdir('img_gifts')
    fonts = os.listdir('fonts')

    for i in range(0, amount):

        # text

        # start_phrase = ['БЕРИ', 'ПОЛУЧАЙ', 'ЗАБИРАЙ', 'БЕСПЛАТНЫЕ', 'ХАЛЯВНЫЕ', 'FREE', 'ТУТ']
        # end_phrase = ['ПОДАРКИ', 'СТИКЕРЫ', 'ОТКРЫТКИ', 'ПОДАРОЧКИ', 'СТИКЕРОЧКИ', 'ПРЕЗЕНТИКИ', 'СМАЙЛИКИ']

        first_word = ['Бери', 'Забирай', 'Качай', 'Ставь', 'Устанавливай', 'Здесь', 'Тут', 'По ссылке', 'По ссылочке',
                      'Кликай', 'Жми']
        second_word = ['подарки', 'подарочки', 'стикеры', 'стикерочки', 'открытки', 'открыточки', 'презентики',
                       'сюрпризы', 'сюрпризики', 'гифки']
        third_word = ['бесплатно', 'за 0 Ок', 'даром', 'на халяву', 'free', 'задаром', 'без денег', 'просто так',
                      'без ОКов', 'на шару']

        # random size of base
        width = random.randint(650, 850)
        height = random.randint(240, 280)

        rrr = random.randint(0, 0)

        # border size
        border_width = width + rrr
        border_height = height + rrr

        # random RGB
        r = random.randint(205, 250)
        g = random.randint(150, 175)
        b = random.randint(120, 230)

        # random RGB
        r_outer = random.randint(105, 210)
        g_outer = random.randint(195, 235)
        b_outer = random.randint(245, 250)

        # text background
        r_text = random.randint(205, 256)
        g_text = random.randint(10, 200)
        b_text = random.randint(180, 230)

        # arrow move
        arrow_move_by_axis_x = random.randint(0, 50)

        # border
        border = Image.new('RGB', (border_width, border_height), (256, 256, 256))

        # text background
        text_background = Image.new('RGB', (width - 80, 60), (r_text, g_text, b_text))

        # text background
        text_background_2 = Image.new('RGB', (width + 5, 65), (256, 256, 256))

        inner_color = [r, g, b]  # Color at the center
        outer_color = [r_outer, g_outer, b_outer]  # Color at the corners

        # base image
        base = Image.new('RGB', (width, height), (r, g, b))

        random_number_for_distance_width = round(random.uniform(1, 2.2), 2)
        random_number_for_distance_height = round(random.uniform(1, 2.2), 2)

        # gradient
        for y in range(height):
            for x in range(width):
                # Find the distance to the center
                distance_to_center = math.sqrt((x - width / random_number_for_distance_width) ** 2 + (y - height / random_number_for_distance_height) ** 2)

                # Make it on a scale from 0 to 1
                distance_to_center = float(distance_to_center) / (math.sqrt(2) * width / random_number_for_distance_width)

                # Calculate r, g, and b values
                r = outer_color[0] * distance_to_center + inner_color[0] * (1 - distance_to_center)
                g = outer_color[1] * distance_to_center + inner_color[1] * (1 - distance_to_center)
                b = outer_color[2] * distance_to_center + inner_color[2] * (1 - distance_to_center)

                # Place the pixel
                base.putpixel((x, y), (int(r), int(g), int(b)))

        random_size = (random.randint(110, 200))

        # get random imgs
        first_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)
        second_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)
        third_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)

        fourth_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)
        fifth_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)
        sixth_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)

        seventh_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)
        eighth_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((random_size, random_size), Image.ANTIALIAS)

        gift_background = Image.new('RGB', (120, 120), (256, 256, 256))

        arrow_numbers = os.listdir('arrows')
        arrow_number = random.choice(arrow_numbers)

        arrow_pic = Image.open('arrows/' + arrow_number).convert('RGBA')
        arrow_pic = arrow_pic.resize((50, 50), Image.ANTIALIAS)

        font = random.choice(fonts)

        fnt = ImageFont.truetype('fonts/' + font, 33)
        fnt2 = ImageFont.truetype('fonts/' + font, 33)

        # get a drawing context
        d = ImageDraw.Draw(border)

        rrr2 = random.randint(0, 10)
        border.paste(base, (0, 0))

        # d.text((round(width * 0.07), round((height*0.25))), random.choice(first_word) + ' '
        #        + random.choice(second_word) + ' '
        #        + random.choice(third_word), font=fnt, fill="#fff")

        rand_height_1 = random.uniform(0.01, 0.68)
        rand_height_2 = random.uniform(0.01, 0.68)
        rand_height_3 = random.uniform(0.01, 0.68)
        rand_height_4 = random.uniform(0.01, 0.68)
        rand_height_5 = random.uniform(0.01, 0.68)
        rand_height_6 = random.uniform(0.01, 0.68)
        rand_height_7 = random.uniform(0.01, 0.68)
        rand_height_8 = random.uniform(0.01, 0.68)

        random_height = random.uniform(0.13, 0.21)
        # border.paste(first_pic, (20, round((height * rand_height_1))), first_pic)
        # border.paste(second_pic, (round(width * 0.25), round((height * rand_height_2))), second_pic)
        border.paste(third_pic, (round(width * 0.5 - random_size + 40), round((height * random_height))), third_pic)
        # border.paste(fourth_pic, (round(width * 0.75), round((height * rand_height_4))), fourth_pic)

        # border.paste(fifth_pic, (20, round((height * rand_height_5))), fifth_pic)
        # border.paste(sixth_pic, (round(width * 0.25), round((height * rand_height_6))), sixth_pic)
        # border.paste(seventh_pic, (round(width * 0.5), round((height * rand_height_7))), seventh_pic)
        # border.paste(eighth_pic, (round(width * 0.75), round((height * rand_height_8))), eighth_pic)

        # border.paste(arrow_pic, (round(width*0 + arrow_move_by_axis_x), 10), arrow_pic)
        # border.putalpha(180)



        # border.paste(text_background_2, (round(width * 0.07), round(height * random_height - 5)))

        # border.paste(text_background, (round(width * 0.07 - 10), round(height*random_height - 10)))
        border.paste(arrow_pic, (round(width * rand_height_1), 10), arrow_pic)

        # text = random.choice(first_word) + '\n' + random.choice(second_word) + '\n' + random.choice(third_word)
        # d.text((round(width * 0.07), round((height * random_height))), text, font=fnt2, fill=(r_outer, g_outer, b_outer))
        # d.text((round(width * 0.07 + 2), round((height * random_height))), text, font=fnt, fill="#fff")

        ra = random.uniform(0.71, 0.91)
        final_pic = border.resize((round(width * ra), round(height * ra)))
        final_pic.save('img_ready/' + str(i + start_index) + '.png', optimize=True, quality=1)
        print(font)


img_gen(amount=1, start_index=0)