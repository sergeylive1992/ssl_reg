import random
import urllib.request
import os

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import math


def save_imgs_from_ok(start_index, path_to_links):
    images_file = open(path_to_links)
    path_to_saved = 'img_gifts/'
    path_where_to_save = 'img_ready/'

    images = images_file.readlines()

    for idx, img in enumerate(images):
        response = urllib.request.urlopen(img)
        urll = str(response.geturl())

        if urll == str(img.split('\n')[0]):
            urllib.request.urlretrieve(img, "saved_imgs/" + str(idx) + ".png")
            print(str(idx) + ' is saved')

        else:
            print('No img')
            pass

    images_file.close()

    filenames = os.listdir(path_to_saved)

    for filename in filenames:
        os.rename(path_to_saved + '/' + filename, path_where_to_save + str(start_index) + '.png')
        start_index += 1


def img_gen(amount, start_index):

    number_of_pics = os.listdir('img_gifts')
    fonts = os.listdir('fonts')

    for i in range(0, amount):

        # text

        # start_phrase = ['БЕРИ', 'ПОЛУЧАЙ', 'ЗАБИРАЙ', 'БЕСПЛАТНЫЕ', 'ХАЛЯВНЫЕ', 'FREE', 'ТУТ']
        # end_phrase = ['ПОДАРКИ', 'СТИКЕРЫ', 'ОТКРЫТКИ', 'ПОДАРОЧКИ', 'СТИКЕРОЧКИ', 'ПРЕЗЕНТИКИ', 'СМАЙЛИКИ']

        first_word = ['Бери', 'Забирай', 'Качай', 'Ставь', 'Устанавливай', 'Здесь', 'Тут', 'По ссылке', 'По ссылочке',
                      'Кликай', 'Жми']
        second_word = ['подарки', 'подарочки', 'стикеры', 'стикерочки', 'открытки', 'открыточки', 'презентики',
                       'сюрпризы', 'сюрпризики', 'гифки']
        third_word = ['бесплатно', 'за 0 Ок', 'даром', 'на халяву', 'free', 'задаром', 'без денег', 'просто так',
                      'без ОКов', 'на шару']

        # random size of base
        width = random.randint(500, 650)
        height = random.randint(240, 320)

        # border size
        border_width = width + 0
        border_height = height + 0

        # random RGB
        r = random.randint(205, 250)
        g = random.randint(150, 175)
        b = random.randint(120, 230)

        # random RGB
        r_outer = random.randint(105, 210)
        g_outer = random.randint(195, 235)
        b_outer = random.randint(245, 250)

        # arrow move
        arrow_move_by_axis_x = random.randint(0, 50)

        # border
        border = Image.new('RGB', (border_width, border_height), (256, 256, 256))

        inner_color = [r, g, b]  # Color at the center
        outer_color = [r_outer, g_outer, b_outer]  # Color at the corners

        # base image
        base = Image.new('RGB', (width, height), (r, g, b))

        random_number_for_distance_width = round(random.uniform(1, 2.2), 2)
        random_number_for_distance_height = round(random.uniform(1, 2.2), 2)

        # gradient
        for y in range(height):
            for x in range(width):
                # Find the distance to the center
                distance_to_center = math.sqrt((x - width / random_number_for_distance_width) ** 2 + (y - height / random_number_for_distance_height) ** 2)

                # Make it on a scale from 0 to 1
                distance_to_center = float(distance_to_center) / (math.sqrt(2) * width / random_number_for_distance_width)

                # Calculate r, g, and b values
                r = outer_color[0] * distance_to_center + inner_color[0] * (1 - distance_to_center)
                g = outer_color[1] * distance_to_center + inner_color[1] * (1 - distance_to_center)
                b = outer_color[2] * distance_to_center + inner_color[2] * (1 - distance_to_center)

                # Place the pixel
                base.putpixel((x, y), (int(r), int(g), int(b)))

        # get random imgs
        first_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA')
        second_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA')
        third_pic = Image.open('img_gifts/' + random.choice(number_of_pics)).convert('RGBA')

        # resize imgs
        first_pic = first_pic.resize((110, 110), Image.ANTIALIAS)
        third_pic = third_pic.resize((110, 110), Image.ANTIALIAS)
        second_pic = second_pic.resize((110, 110), Image.ANTIALIAS)
        gift_background = Image.new('RGB', (120, 120), (256, 256, 256))

        arrow_numbers = os.listdir('arrows')
        arrow_number = random.choice(arrow_numbers)

        arrow_pic = Image.open('arrows/' + arrow_number).convert('RGBA')
        arrow_pic = arrow_pic.resize((50, 50), Image.ANTIALIAS)

        font = random.choice(fonts)
        fnt = ImageFont.truetype('fonts/' + font, 33)

        # get a drawing context
        d = ImageDraw.Draw(border)

        border.paste(base, (0, 0))

        d.text((round(width * 0.07), round((height*0.25))), random.choice(first_word) + ' '
               + random.choice(second_word) + ' '
               + random.choice(third_word), font=fnt, fill="#fff")

        number_of_pics_random = random.randint(0,3)

        if number_of_pics_random == 1:
            border.paste(gift_background, (round(width*0.37 - 5), round((height*0.4 - 5))))
            border.paste(second_pic, (round(width*0.37), round((height*0.4))), second_pic)

        elif number_of_pics_random == 2:
            border.paste(gift_background, (25, round((height * 0.5 - 5))))
            border.paste(first_pic, (30, round((height * 0.5))), first_pic)
            border.paste(gift_background, (round(width * 0.7 - 5), round((height * 0.5 - 5))))
            border.paste(third_pic, (round(width*0.7), round((height * 0.5))), third_pic)

        elif number_of_pics_random == 3:
            border.paste(first_pic, (30, round((height * 0.5))), first_pic)
            border.paste(second_pic, (round(width*0.37), round((height * 0.5))), second_pic)
            border.paste(third_pic, (round(width*0.7), round((height * 0.5))), third_pic)
        elif number_of_pics_random == 0:
            border.paste(second_pic, (round(width * 0.37), round((height * 0.5))), second_pic)
            border.paste(third_pic, (round(width * 0.7), round((height * 0.5))), third_pic)

        border.paste(arrow_pic, (round(width*0 + arrow_move_by_axis_x), 10), arrow_pic)

        border.save('img_ready/' + str(i + start_index) + '.png', 'PNG')


img_gen(amount=1, start_index=0)
