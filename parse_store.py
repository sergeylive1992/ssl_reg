import time
from selenium import webdriver

driver = webdriver.Chrome()
driver.implicitly_wait(3)

driver.get('https://chrome.google.com/webstore/category/ext/13-sports')

SCROLL_PAUSE_TIME = 2

# Get scroll height
last_height = driver.execute_script("return document.body.scrollHeight")


i = 1
while True:
    print(str(i))
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(SCROLL_PAUSE_TIME)

    # Calculate new scroll height and compare with last scroll height
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        break
    last_height = new_height
    i = i + 1

ext_links = driver.find_elements_by_class_name('a-u')

links = []
for ext in ext_links:
    link = ext.get_attribute('href')
    links.append(link)

    print(link)

driver.close()
driver.quit()
