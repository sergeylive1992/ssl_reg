import os

path = 'saved_imgs/'

filenames = os.listdir(path)

i = 0

for filename in filenames:
    os.rename(path + '/' + filename, 'imgs_original/' + str(i) + '.png')
    i += 1