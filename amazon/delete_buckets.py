import boto3


client = boto3.client('s3')
s3 = boto3.resource('s3')
response = client.list_buckets()

bucket_names = []
for i in response['Buckets']:
    bucket_names.append([i['Name'], str(i['CreationDate'].date()), str(i['CreationDate'].time())])

a = [j for j in bucket_names]
print(len(a))

for b in a:
    bucket = s3.Bucket(b[0])
    print('Bucket ' + b[0] + ' is deleting...')
    for key in bucket.objects.all():
        print(key)
        key.delete()
    bucket.delete()
    print('Bucket ' + b[0] + ' is deleted')

