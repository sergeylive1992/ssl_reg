from shutil import copyfile
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove

import boto3
import random
import os
import threading

import time

start_time = time.time()

# Connect to s3 and create bucket
s3 = boto3.resource('s3')


def upload_file_html(links_number, thread, domains_count, html_pattern):

    # Links file
    links_amazon = open('links_amazon_' + str(thread) + '.txt', 'w+')

    # File with words
    words_file = open('words_2.txt', 'r')
    words = words_file.readlines()

    for domain in range(0, domains_count):

        # Random words
        random_words = []
        for i in range(0, links_number + 10000):
            r1 = random.randint(0, len(words) - 1)
            r2 = random.randint(0, len(words) - 1)
            word = words[r1].split('\n')[0].lower() + words[r2].split('\n')[0].lower()
            random_words.append(word)

        html_location = 'html/thread_' + str(thread) + '/'

        def rename_file_html():
            if not os.path.exists(html_location):
                os.makedirs(html_location)
                copyfile(html_pattern + '.html', html_location + '/' + html_pattern + '.html')

            # Find and rename html file
            for html_file in os.listdir(html_location):
                if html_file.endswith('.html'):

                    r = random.randint(0, len(random_words))
                    html_file_path = os.path.join(html_location, html_file)
                    os.rename(html_file_path, html_location + random_words[r - 1] + '.html')

                    # Path to file
                    html_file_path = html_location + random_words[r - 1] + '.html'
                    return html_file_path

        # Random bucket name
        bucket_name = random_words[domain]

        # print(random_words)

        for j in range(0, links_number):

            s3.create_bucket(Bucket=bucket_name)

            html_file = rename_file_html()

            # Path to file
            data = open(html_file, 'rb')

            r = random.randint(10, len(random_words))

            try:
                # Path to file on amazon
                # bucket_path = random_words[r - 1] + '/' + random_words[r - 2] + '/' + html_file.split(html_location)[1]
                bucket_path = random_words[r - 1]

            except:
                print('Error on %s and %s' % (str(r), str(r - 1)))
                pass

            bucket = s3.Bucket(bucket_name)
            # print(bucket_path)

            # Upload object with content type
            bucket.put_object(Key=bucket_path, Body=data, ACL='public-read', ContentType='text/html')

            # link = 'https://' + bucket_name + '.s3.amazonaws.com/' + bucket_path
            link = 'https://' + bucket_name + '.s3.amazonaws.com/' + bucket_path
            print('Thread_' + str(thread) + ' : ' + str(j))

            links_amazon.write(link + '\n')

    # Close files
    words_file.close()
    links_amazon.close()


# # init events
# e1 = threading.Event()


# init threads
t1 = threading.Thread(target=upload_file_html, args=(1, 1, 1, 'new_land'))


# start threads
t1.start()


end_time = time.time()

result = end_time - start_time
print(str(round(result/60)) + 'm')
