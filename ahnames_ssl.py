import getpass
import os

import time
import zipfile

from selenium import webdriver


user = getpass.getuser()

driver = webdriver.Chrome(os.getcwd() + '/' + 'chromedriver')
driver.implicitly_wait(10)


domain_to_reg = 'ontouwer'
ah_id = 18817342

ah_login = 'pfanera'
ah_password = 'sakp89nAme!s'

sslfree_login = 'fedot-efimov@bk.ru'
sslfree_password = 'O4hjhgJpY'

path_to_ssl = 'SSL/'
path_to_free_ssl = 'ok_ssl/'

link_to_reg = 'https://www.sslforfree.com/create?domains=' + domain_to_reg + '.ru'

# login to ssl-for-free
driver.get('https://www.sslforfree.com/login')
driver.find_element_by_xpath('//*[@id="content_login"]/form/label[1]/input').send_keys(sslfree_login)
driver.find_element_by_xpath('//*[@id="content_login"]/form/label[2]/input').send_keys(sslfree_password)
driver.find_element_by_xpath('//*[@id="content_login"]/form/button[2]').click()
print(link_to_reg)
time.sleep(2)

# reg ssl and get data for verification
driver.get(link_to_reg)
driver.find_element_by_xpath('//*[@id="content_create"]/div/div[1]/a[3]').click()
driver.find_element_by_id('content_create_manual_dns_form').click()

# data for verification
host = driver.find_element_by_xpath('//*[@id="content_create_manual_dns_output"]/ol/li[2]/ol/li/strong[1]').text
txt_record = driver.find_element_by_xpath('//*[@id="content_create_manual_dns_output"]/ol/li[2]/ol/li/strong[2]').text


def add_txt_ahnames(login, password, domain_id):

    driver = webdriver.Chrome(os.getcwd() + '/' + 'chromedriver')
    driver.set_window_size(1800, 1800)

    driver.get('https://hiam.ahnames.com/site/login')
    driver.find_element_by_id('loginform-username').send_keys(login)
    driver.find_element_by_id('loginform-password').send_keys(password)
    driver.find_element_by_xpath('//*[@id="login-form"]/div[3]/div[2]/button').click()

    time.sleep(1)

    driver.find_element_by_xpath('/html/body/div[2]/div[3]/div[1]/a').click()

    driver.get('https://hipanel.ahnames.com/domain/domain/view?id=' + str(domain_id))

    time.sleep(2)

    driver.find_element_by_xpath('//*[@id="domain-details-tab"]/li[4]').click()

    time.sleep(4)

    driver.find_element_by_xpath('//*[@id="record-0-name"]').send_keys('_acme-challenge')
    driver.find_element_by_xpath('//*[@id="record-0-type"]/option[4]').click()
    driver.find_element_by_xpath('//*[@id="record-0-value"]').send_keys(txt_record)

    time.sleep(2)

    driver.find_element_by_xpath('//*[contains(@id,"dynamic-form-")]/div/div/div[2]/div[1]/button[1]').click()

    time.sleep(4)

    driver.close()
    driver.quit()


add_txt_ahnames(ah_login, ah_password, ah_id)

time.sleep(5)

driver.find_element_by_xpath('//*[@id="content_create_manual_dns_output"]/form/button').click()

time.sleep(3)

driver.find_element_by_xpath('//*[@id="certificate_download"]').click()

time.sleep(3)

zip_ref = zipfile.ZipFile('/home/' + user + '/Downloads/sslforfree.zip', 'r')
zip_ref.extractall('/home/' + user + '/Documents/work_scripts/SSL')
zip_ref.close()

zip_file_name = path_to_free_ssl + domain_to_reg + '_ru_crt.zip'
myZipFile = zipfile.ZipFile(zip_file_name, 'w')
myZipFile.write(path_to_ssl + 'ca_bundle.crt', 'ca_1.crt', zipfile.ZIP_DEFLATED)
myZipFile.write(path_to_ssl + 'certificate.crt', domain_to_reg + '_ru.crt', zipfile.ZIP_DEFLATED)
myZipFile.write(path_to_ssl + 'private.key', domain_to_reg + '.ru.key', zipfile.ZIP_DEFLATED)

driver.close()
driver.quit()

os.remove('/home/' + user + '/Downloads/sslforfree.zip')

print('SSL cert was successfully created')