import requests
from bs4 import BeautifulSoup

file_with_sites = open('sites_for_parts')
sites = file_with_sites.readlines()

script_link = 'https://res.cloudinary.com/drozrk6t8/raw/upload/v1526473262/123_qsg31z.js'

for site in sites[:2]:
    try:
        r = requests.get('http://' + site.split('\n')[0], timeout=5)
        if r.status_code == 200:
            file = open('sites_html/p_' + site.split('\n')[0] + '.html', 'w+')

            print(site.split('\n')[0] + ' is scrapping...' + '\n')

            soup = BeautifulSoup(r.content, 'lxml')

            # finding of first script tag
            script_tag = soup.find('body')

            # creating of new script tag with properties
            new_frame = soup.new_tag('script')
            new_frame['src'] = script_link

            # script_tag.insert_before(new_frame)
            soup.body.insert(len(soup.body.contents), new_frame)

            file.write(str(soup))

            print('ok for ' + site.split('\n')[0])

            file.close()

    except Exception as e:
        print(e)

print('Done')