import datetime
import os

import imageio as imageio
from PIL import Image


def extract_frames(in_gif, out_folder):
    frame = Image.open(in_gif)
    n_frames = 0
    while frame:
        frame.save('%s/%s-%s.png' % (out_folder, os.path.basename(in_gif), n_frames), 'PNG')
        n_frames += 1
        try:
            frame.seek(n_frames)
        except EOFError:
            break
    return True


# extract_frames('test.gif', 'output')

def create_gif(filenames, duration):
    images = []
    for filename in filenames:
        images.append(imageio.imread(filename))
    output_file = 'Gif.gif'
    imageio.mimsave(output_file, images, duration=duration)

images = []
for i in os.listdir('/home/deka/Documents/work_scripts/cards/output/result/'):
    file = '/home/deka/Documents/work_scripts/cards/output/result/' + i
    images.append(file)

print(images)
create_gif(images, 0.4)