import cv2 as cv

from moviepy.editor import *
from PIL import Image
from wand.image import Image as imag


gifs = 'cards/Времена года/Весна/3.gif'


def replace_watermark(gif, out_folder):
    frame = Image.open(gif)
    n_frames = 0
    frame.save('%s/%s.png' % (out_folder, os.path.basename(gif)), 'PNG')
    frame.seek(n_frames)

    path_to_frame = 'output/' + os.path.basename(gif) + '.png'
    img = cv.imread(path_to_frame, 0)
    print('output/' + os.path.basename(gif))
    img2 = img.copy()
    template = cv.imread('template_3.png', 0)
    template_2 = cv.imread('template.png', 0)
    w, h = template.shape[::-1]

    methods = ['cv.TM_CCOEFF_NORMED']

    for meth in methods:

        img = img2.copy()
        method = eval(meth)

        # Apply template Matching
        res = cv.matchTemplate(img, template, method)
        min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)
        print(max_val)

        if max_val < 0.5:

            print('Use another template')
            res_2 = cv.matchTemplate(img, template_2, method)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res_2)
            # print(max_val)

        if max_val < 0.5:
            break

        print(max_val)

        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc

        bottom_right = (top_left[0] + w, top_left[1] + h)

        cv.rectangle(img, top_left, bottom_right, 255, 2)

        top_left_final = (top_left[0] - 2, top_left[1] - 4)

        gif_clip = VideoFileClip(gif, audio=False, audio_buffersize=0, audio_nbytes=0).speedx(1.8)
        logo = (ImageClip('logo_mini_2.png')
                .set_duration(gif_clip.duration)
                .set_opacity(1)
                .set_position(top_left_final))
        final_clip = CompositeVideoClip([gif_clip, logo])

        gif_name = gif.split('/')[-1]
        gif_d = gif.split(gif_name)[0].replace('cards','cards_new')

        final_clip.write_gif(gif_d + gif_name, fps=7, fuzz=2, program='ImageMagick', opt='OptimizeTransparency', progress_bar=False)
        final_clip.close()

        # create thumb for gif
        img = imag(filename=gif)
        img.resize(280, 200)
        gif_res = gif.replace('cards','cards_new').split('.gif')[0] + '_thumb.gif'
        print('\n' + '------------------' + gif_res)
        img.save(filename=gif_res)
        img.close()

        os.remove(path_to_frame)


replace_watermark(gifs, 'output')
