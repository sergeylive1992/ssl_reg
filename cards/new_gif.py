import random

from PIL import Image, ImageFont, ImageDraw
from moviepy.editor import *


def create_gif(filenames, duration, name):

    images = []
    for filename in filenames:
        images.append(imageio.imread(filename))

    output_file = 'C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\share\\' + name + '.gif'
    imageio.mimsave(output_file, images[0:5], duration=duration)

    try:
        gif_clip = VideoFileClip(output_file, audio=False, audio_buffersize=0,
                                 audio_nbytes=0)
        gif_clip.write_gif('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\share\\' + name + '.gif', fps=3, fuzz=5,
                           program='ImageMagick', opt='optimizeplus', progress_bar=False)
    except:
        print('Not ok')


def img_gen(amount, start_index):
    number_of_pics = os.listdir('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\images_ok\\')

    a = ['   Жми', '   Тут', 'По ссылке', '  Качай', '   Дари', '   Бери', 'На сайте', '  Кликай']
    b = ['подарки', 'подарочки', 'открытки', 'открыточки', 'стикеры', 'гифки']
    c = ['бесплатно', 'без ОКов', 'на шару']

    text1 = random.choice(a)
    text = '\n\n' + random.choice(b).upper() + '\n' + random.choice(c).upper()

    arrow_pics = os.listdir('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\arrows\\')
    arrow_pic = Image.open(
        'C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\arrows\\' + random.choice(arrow_pics)).convert('RGBA')

    backgrounds = os.listdir('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\backgrounds')

    fonts = os.listdir('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\fonts')

    font = 'C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\fonts\\' + random.choice(fonts)

    # Font
    fnt = ImageFont.truetype(font, 45)

    fnt_2 = ImageFont.truetype(font, 73)

    background = random.choice(backgrounds)

    print(background)

    for i in range(0, amount):

        border = Image.open('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\backgrounds\\' + background).convert(
            'RGBA')

        gift_pic = Image.open(
            'C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\images_ok\\' + random.choice(number_of_pics)).convert(
            'RGBA').resize((190, 190), Image.ANTIALIAS)

        arrow_pic = arrow_pic.resize((55, 55), Image.ANTIALIAS)

        border.paste(arrow_pic, (round(border.width * 0.2), 10), arrow_pic)

        if background == '1.png':

            # Draw text
            d = ImageDraw.Draw(border)
            d.text((100, 80), text1, font=fnt, fill=(255, 255, 255, 255))
            d.text((55, 110), text, font=fnt_2, fill=(255, 255, 255, 255))

            border.paste(gift_pic, (round(border.width * 0.65), 130), gift_pic)

        elif background == '2.png':

            # Draw text
            d = ImageDraw.Draw(border)
            d.text((100, 80), text1, font=fnt, fill=(255, 116, 0, 255))
            d.text((50, 110), text, font=fnt_2, fill=(255, 116, 0, 255))

            border.paste(gift_pic, (round(border.width * 0.65), 130), gift_pic)

        elif background == '3.png':

            # Draw text
            d = ImageDraw.Draw(border)
            d.text((100, 80), text1, font=fnt, fill=(0, 212, 4, 255))
            d.text((50, 70), text, font=fnt_2, fill=(255, 116, 0, 255))

            border.paste(gift_pic, (round(border.width * 0.65), 130), gift_pic)

        elif background == '4.png':

            # Draw text
            d = ImageDraw.Draw(border)
            d.text((100, 80), text1, font=fnt, fill=(255, 255, 255, 255))
            d.text((50, 70), text, font=fnt_2, fill=(255, 255, 255, 255))

            border.paste(gift_pic, (round(border.width * 0.65), 130), gift_pic)

        elif background == '5.png':

            # Draw text
            d = ImageDraw.Draw(border)
            d.text((100, 80), text1, font=fnt, fill=(255, 255, 255, 255))
            d.text((50, 70), text, font=fnt_2, fill=(255, 255, 255, 255))

            border.paste(gift_pic, (round(border.width * 0.65), 130), gift_pic)

        elif background == '6.png':

            # Draw text
            d = ImageDraw.Draw(border)
            d.text((100, 70), text1, font=fnt)
            d.text((50, 150), text, font=fnt_2, fill=(255, 255, 255, 255))

            border.paste(gift_pic, (round(border.width * 0.65), 130), gift_pic)

        elif background == '7.png':

            # Draw text
            d = ImageDraw.Draw(border)
            d.text((100, 80), text1, font=fnt, fill=(255, 255, 255, 255))
            d.text((50, 110), text, font=fnt_2, fill=(255, 255, 255, 255))

            border.paste(gift_pic, (round(border.width * 0.62), 210), gift_pic)

        border.save('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\share\\result\\' + str(i + start_index) + '.png',
                    'PNG', optimize=True, quality=5)


for k in range(0, 1001):

    # gen images
    img_gen(amount=4, start_index=0)

    images = []
    for i in os.listdir('C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\share\\result\\'):
        file = 'C:\\Users\\tutu\\Documents\\ssl_reg\\ok_images\\share\\result\\' + i
        images.append(file)

    # create gif
    create_gif(images, 0.6, str(k))
