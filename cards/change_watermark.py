# https://stackoverflow.com/questions/14767594/how-to-detect-object-on-images

import cv2 as cv
import os

import imageio
from PIL import Image


def create_gif(filenames, output_file, duration):
    images = []
    for filename in filenames:
        images.append(imageio.imread(filename))
    imageio.mimsave(output_file, images, duration=duration)


def extract_frames(in_gif, out_folder):
    frame = Image.open(in_gif)
    n_frames = 0
    while frame:
        frame.save('%s/%s-%s.png' % (out_folder, os.path.basename(in_gif), n_frames), 'PNG')
        n_frames += 1
        try:
            frame.seek(n_frames)
        except EOFError:
            break
    return True


extract_frames('test.gif', 'output')


for pic in os.listdir(os.getcwd() + '/output/'):
    if pic.endswith('.png'):
        img = cv.imread('/home/deka/Documents/work_scripts/cards/output/' + pic, 0)
        print('/home/deka/Documents/work_scripts/cards/output/' + pic)
        img2 = img.copy()
        template = cv.imread('/home/deka/Documents/work_scripts/cards/template.png', 0)
        w, h = template.shape[::-1]

        # All the 6 methods for comparison in a list
        methods = ['cv.TM_CCOEFF']

        for meth in methods:
            img = img2.copy()
            method = eval(meth)

            # Apply template Matching
            res = cv.matchTemplate(img,template,method)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)

            # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
            if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
                top_left = min_loc
            else:
                top_left = max_loc

            bottom_right = (top_left[0] + w, top_left[1] + h)

            cv.rectangle(img,top_left, bottom_right, 255, 2)

            print(top_left)

            image = Image.open('/home/deka/Documents/work_scripts/cards/output/' + pic).convert('RGBA')
            template_my = Image.open('/home/deka/Documents/work_scripts/cards/template_my.png').convert('RGBA')

            image.paste(template_my, top_left)
            image.save('/home/deka/Documents/work_scripts/cards/output/result/' + pic)

    images = []
    for i in os.listdir('/home/deka/Documents/work_scripts/cards/output/result/'):
        file = '/home/deka/Documents/work_scripts/cards/output/result/' + i
        images.append(file)
    create_gif(images, 'gif.gif', 1)

