import random

import imageio
import os

import math

import time
from PIL import Image, ImageFont, ImageDraw
from moviepy.video.io.VideoFileClip import VideoFileClip

start_time = time.time()


def create_gif(filenames, duration, name):
    images = []
    for filename in filenames:
        images.append(imageio.imread(filename))
    output_file = 'share/' + name + '.gif'
    imageio.mimsave(output_file, images[0:5], duration=duration)

    gif_clip = VideoFileClip('/home/deka/Documents/work_scripts/cards/share/' + name + '.gif', audio=False, audio_buffersize=0,
                             audio_nbytes=0)
    gif_clip.write_gif('share/' + name + '.gif', fps=7, fuzz=2, program='ImageMagick', opt='OptimizeTransparency', progress_bar=False)


def img_gen(amount, start_index):

    number_of_pics = os.listdir('/home/deka/Documents/work_scripts/ok_images/img_gifts')
    fonts = os.listdir('/home/deka/Documents/work_scripts/ok_images/fonts')

    a = ['Скачай', 'Cкaчaй', 'Закачай', '3aкaчaй', 'Установи', 'Уcтaнoви']
    b = ['здесь', 'здecь', 'тут', 'тyт', 'на этом сайте', 'нa этoм caйтe', 'по ссылке', 'no ccылкe']
    c = ['расширение', 'pacшиpeниe', 'дополнение', 'дonoлнeниe', 'приложение', 'пpилoжeниe']
    d = ['так же', 'тaк жe', 'тоже', 'тoжe']
    dd = ['сможешь', 'cмoжeшь', 'получишь возможность', 'noлyчишь вoзмoжнocть']
    e = ['отправлять', 'oтпpaвлять', 'слать', 'cлaть', 'присылать', 'npиcылaть']
    f = ['все', 'всe', 'любые', 'любыe']
    g = ['стикеры', 'cтикepы', 'открытки', 'oткpытки', 'подарки', 'noдapки']
    h = ['бесплатно', 'бecплaтнo', 'без пополнения счета в ОК', 'бeз nonoлнeния cчeтa в OK', 'без ОКов', 'бeз OKoв',
         'без ОКов', 'на шару']

    text = random.choice(a) + ' себе ' + random.choice(b) + '\n' + random.choice(c) + ' ' + ' и ' + '\n' + \
           random.choice(d) + ' ' + random.choice(dd) + '\n' + random.choice(e) + ' ' + random.choice(f) + ' ' + \
           random.choice(g) + '\n' + random.choice(h) + '!'

    for i in range(0, amount):

        # random size of base
        width = 700
        height = 500

        rrr = random.randint(0, 0)

        # border size
        border_width = width + rrr
        border_height = height + rrr

        # random RGB
        r = random.randint(230, 250)
        g = random.randint(70, 90)
        b = random.randint(40, 60)

        # random RGB
        r_outer = random.randint(170, 190)
        g_outer = random.randint(200, 210)
        b_outer = random.randint(200, 210)

        # border
        border = Image.new('RGB', (border_width, border_height), (256, 256, 256))

        inner_color = [r, g, b]  # Color at the center
        outer_color = [r_outer, g_outer, b_outer]  # Color at the corners

        # base image
        base = Image.new('RGB', (width, height), (r, g, b))

        random_number_for_distance_width = round(random.uniform(1, 2.2), 2)
        random_number_for_distance_height = round(random.uniform(1, 2.2), 2)

        # gradient
        for y in range(height):
            for x in range(width):
                # Find the distance to the center
                distance_to_center = math.sqrt((x - width / random_number_for_distance_width) ** 2 + (y - height / random_number_for_distance_height) ** 2)

                # Make it on a scale from 0 to 1
                distance_to_center = float(distance_to_center) / (math.sqrt(2) * width / random_number_for_distance_width)

                # Calculate r, g, and b values
                r = outer_color[0] * distance_to_center + inner_color[0] * (1 - distance_to_center)
                g = outer_color[1] * distance_to_center + inner_color[1] * (1 - distance_to_center)
                b = outer_color[2] * distance_to_center + inner_color[2] * (1 - distance_to_center)

                # Place the pixel
                base.putpixel((x, y), (int(r), int(g), int(b)))

        font = fonts[0]
        fnt = ImageFont.truetype('/home/deka/Documents/work_scripts/ok_images/fonts/' + font, 45)

        # get a drawing context
        d = ImageDraw.Draw(border)

        border.paste(base, (0, 0))

        d.text((round(width * 0.07), round((height * 0.25))), text, font=fnt, fill="#fff")

        rand_height_1 = 20

        arrow_pic = Image.open('/home/deka/Documents/work_scripts/ok_images/arrows/arrow2.png').convert('RGBA')

        gift_pic = Image.open('/home/deka/Documents/work_scripts/ok_images/img_gifts/' + random.choice(number_of_pics)).convert('RGBA').resize((150, 150), Image.ANTIALIAS)
        arrow_pic = arrow_pic.resize((50, 50), Image.ANTIALIAS)
        border.paste(arrow_pic, (20, rand_height_1 - 1), arrow_pic)
        border.paste(gift_pic, (round(width*0.75), 110), gift_pic)

        border.save('/home/deka/Documents/work_scripts/cards/output/result/' + str(i + start_index) + '.png', 'PNG', optimize=True, quality=40)


for k in range(0, 300):

    images = []
    for i in os.listdir('/home/deka/Documents/work_scripts/cards/output/result/'):
        file = '/home/deka/Documents/work_scripts/cards/output/result/' + i
        images.append(file)

    # gen images
    img_gen(amount=5, start_index=0)

    # create gif
    create_gif(images[:3], 0.6, str(k))

end_time = time.time()

result = end_time - start_time

print('\n' + str(round(result)) + ' sec')

