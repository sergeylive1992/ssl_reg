import cv2 as cv

from moviepy.editor import *
from PIL import Image


gifs = [os.getcwd() + '/test_cards/' + a for a in os.listdir('test_cards')]
print(gifs)


def replace_watermark(in_gif, out_folder):
    for idx, gif in enumerate(in_gif):
        print(gif)
        frame = Image.open(gif)
        n_frames = 0
        frame.save('%s/%s.png' % (out_folder, os.path.basename(gif)), 'PNG')
        frame.seek(n_frames)

        path_to_frame = 'output/' + os.path.basename(gif) + '.png'
        img = cv.imread(path_to_frame, 0)
        print('output/' + os.path.basename(gif))
        img2 = img.copy()
        template = cv.imread('template.png', 0)
        w, h = template.shape[::-1]

        # All the 6 methods for comparison in a list
        # methods = ['cv.TM_CCOEFF', 'cv.TM_CCOEFF_NORMED', 'cv.TM_CCORR',
        #            'cv.TM_CCORR_NORMED', 'cv.TM_SQDIFF', 'cv.TM_SQDIFF_NORMED']
        methods = ['cv.TM_CCOEFF_NORMED']

        for meth in methods:
            img = img2.copy()
            method = eval(meth)

            # Apply template Matching
            res = cv.matchTemplate(img,template,method)
            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)

            # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
            if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
                top_left = min_loc
            else:
                top_left = max_loc

            bottom_right = (top_left[0] + w, top_left[1] + h)

            cv.rectangle(img, top_left, bottom_right, 255, 2)

            # plt.subplot(121), plt.imshow(res, cmap='gray')
            # plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
            # plt.subplot(122), plt.imshow(img, cmap='gray')
            # plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
            # plt.suptitle(meth)
            # plt.show()

            top_left_final = (top_left[0] - 2, top_left[1] - 4)

            gif_clip = VideoFileClip(gif, audio=False, audio_buffersize=0, audio_nbytes=0).speedx(1.8)
            logo = (ImageClip('logo_mini_2.png')
                    .set_duration(gif_clip.duration)
                    .set_opacity(1)
                    .set_position(top_left_final))
            final_clip = CompositeVideoClip([gif_clip, logo])

            gif_name = str(idx)

            final_clip.write_gif(gif_name + '.gif', fps=7, fuzz=2, program='ImageMagick', opt='OptimizeTransparency', progress_bar=False)

            os.remove(path_to_frame)


replace_watermark(gifs, 'output')
