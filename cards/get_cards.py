import time
import urllib.request

from selenium import webdriver
import os


categories = {
   # "Праздники":"https://otkritkiok.ru/prazdniki",
   # "День рождение женщине":{
   #    "Женщине":"https://otkritkiok.ru/den-rozhdeniya/zhenshchine",
   #    "Подруге":"https://otkritkiok.ru/den-rozhdeniya/podruge",
   #    "Девушке":"https://otkritkiok.ru/den-rozhdeniya/devushke",
   #    "Маме":"https://otkritkiok.ru/den-rozhdeniya/mame",
   #    "Бабушке":"https://otkritkiok.ru/den-rozhdeniya/babushke",
   #    "Жене":"https://otkritkiok.ru/den-rozhdeniya/zhene",
   #    "Сестре":"https://otkritkiok.ru/den-rozhdeniya/sestre",
   #    "Тете":"https://otkritkiok.ru/den-rozhdeniya/tete",
   #    "Дочери":"https://otkritkiok.ru/den-rozhdeniya/docheri"
   # },
   # "День рождение мужчине":{
   #    "Мужчине":"https://otkritkiok.ru/den-rozhdeniya/muzhchine",
   #    "Другу":"https://otkritkiok.ru/den-rozhdeniya/drugu",
   #    "Папе":"https://otkritkiok.ru/den-rozhdeniya/pape",
   #    "Дедушке":"https://otkritkiok.ru/den-rozhdeniya/dedushke",
   #    "Мужу":"https://otkritkiok.ru/den-rozhdeniya/muzhu",
   #    "Брату":"https://otkritkiok.ru/den-rozhdeniya/bratu",
   #    "Дяде":"https://otkritkiok.ru/den-rozhdeniya/dyade",
   #    "Сыну":"https://otkritkiok.ru/den-rozhdeniya/synu"
   # },
   # "Ежедневные":{
   #    "Привет":"https://otkritkiok.ru/ejednevnie/privet",
   #    "Настроения":"https://otkritkiok.ru/ejednevnie/haroshego-nastroenia",
   #    "Спасибо":"https://otkritkiok.ru/ejednevnie/spasibo",
   #    "Доброе утро":"https://otkritkiok.ru/ejednevnie/dobroe-utro",
   #    "Добрый день":"https://otkritkiok.ru/ejednevnie/dobrii-deni",
   #    "Добрый вечер":"https://otkritkiok.ru/ejednevnie/dobrii-vecer",
   #    "Хорошего дня":"https://otkritkiok.ru/ejednevnie/horoshego-dnya",
   #    "Хорошего вечера":"https://otkritkiok.ru/ejednevnie/horoshego-vecera",
   #    "Спокойной ночи":"https://otkritkiok.ru/ejednevnie/spokoynoy-nochi"
   # },
   # "Подруге":"https://otkritkiok.ru/pojelanie",
   # "Пожелания":"https://otkritkiok.ru/podruge",
   # "Другу":"https://otkritkiok.ru/drugu",
   # "Друзьям":"https://otkritkiok.ru/druziam",
   # "Цветы":"https://otkritkiok.ru/tsveti",
   # "Розы":"https://otkritkiok.ru/rozi",
   # "Религия":"https://otkritkiok.ru/religia",
   # "Любовь и романтика":"https://otkritkiok.ru/love",
   # "Важные события":{
   #    "Годовщина свадьбы":"https://otkritkiok.ru/vazhno/godovshina-svadbi",
   #    "С новорожденным":"https://otkritkiok.ru/vazhno/novorojdennim",
   #    "Свадьба":"https://otkritkiok.ru/vazhno/svadba",
   #    "С новосельем":"https://otkritkiok.ru/vazhno/novosele",
   #    "С пенсией":"https://otkritkiok.ru/vazhno/pensiya"
   # },
   "Времена года":{
      "Весна":"https://otkritkiok.ru/vesna",
      "Лето":"https://otkritkiok.ru/leto",
      "Осень":"https://otkritkiok.ru/oseni",
      "Зима":"https://otkritkiok.ru/zima"
   }
}

SCROLL_PAUSE_TIME = 0.5

driver = webdriver.Chrome('/home/deka/Documents/work_scripts/chromedriver')


for category in categories:
    # if type(categories[category]) != dict:
    #     print(category + ' ' + categories[category])
    #
    #     driver.get(categories[category])
    #
    #     # Get scroll height
    #     last_height = driver.execute_script("return document.body.scrollHeight")
    #
    #     while True:
    #         # Scroll down to bottom
    #         driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    #
    #         # Wait to load page
    #         time.sleep(SCROLL_PAUSE_TIME)
    #
    #         # Calculate new scroll height and compare with last scroll height
    #         new_height = driver.execute_script("return document.body.scrollHeight")
    #         if new_height == last_height:
    #             break
    #         last_height = new_height
    #
    #     gs = driver.find_elements_by_css_selector('.thumbnail.ok-cardThumbnail > img')
    #     for idx, g in enumerate(gs):
    #         link = g.get_attribute('src').replace('thumbs', 'big')
    #         response = urllib.request.urlopen(link)
    #         print(link)
    #         urllib.request.urlretrieve(link, 'cards/' + category + '/' + str(idx) + '.gif')

    if type(categories[category]) == dict:
        for sub_category in categories[category]:
            # os.mkdir('cards/' + category + '/' + sub_category)

            driver.get(categories[category][sub_category])

            # Get scroll height
            last_height = driver.execute_script("return document.body.scrollHeight")

            while True:
                # Scroll down to bottom
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

                # Wait to load page
                time.sleep(SCROLL_PAUSE_TIME)

                # Calculate new scroll height and compare with last scroll height
                new_height = driver.execute_script("return document.body.scrollHeight")
                if new_height == last_height:
                    break
                last_height = new_height

            print(category + ' ' + sub_category + ' ' + categories[category][sub_category])

            gs = driver.find_elements_by_css_selector('.thumbnail.ok-cardThumbnail > img')
            for idx, g in enumerate(gs):
                link = g.get_attribute('src').replace('thumbs', 'big')
                response = urllib.request.urlopen(link)
                print(link)
                urllib.request.urlretrieve(link, 'cards/' + category + '/' + sub_category + '/' + str(idx) + '.gif')

