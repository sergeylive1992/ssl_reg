from moviepy.editor import *
from cards.replace_w import replace_watermark


paths = os.listdir('cards/')

for path in paths:
    pp = os.listdir('cards/' + path)
    for p in pp:
        if p.endswith('.gif'):
            gif = 'cards/' + path + '/' + p
            replace_watermark(gif, 'output')
        else:
            sub_pp = os.listdir('cards/' + path + '/' + p)
            for sub_p in sub_pp:
                gif = 'cards/' + path + '/' + p + '/' + sub_p
                print('cards/' + path + '/' + p + '/' + sub_p)
                replace_watermark(gif, 'output')
