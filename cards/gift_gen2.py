import random
import threading

import imageio
import os

import math

import time
from PIL import Image, ImageFont, ImageDraw
from moviepy.video.io.VideoFileClip import VideoFileClip

# start_time = time.time()


def create_gif(filenames, duration, name):
    images = []
    for filename in filenames:
        images.append(imageio.imread(filename))
    output_file = 'share/' + name + '.gif'
    imageio.mimsave(output_file, images[0:5], duration=duration)

    gif_clip = VideoFileClip('/home/deka/Documents/work_scripts/cards/share/' + name + '.gif', audio=False, audio_buffersize=0,
                             audio_nbytes=0)
    gif_clip.write_gif('share/' + name + '.gif', fps=7, fuzz=2, program='ImageMagick', opt='OptimizeTransparency', progress_bar=False)


def img_gen(amount, start_index):

    number_of_pics = os.listdir('/home/deka/Documents/work_scripts/ok_images/img_gifts')
    fonts = os.listdir('/home/deka/Documents/work_scripts/ok_images/fonts')

    a = ['Жми', 'Тут', 'По ссылке', 'Качай', 'Дари', 'Бери', 'На сайте', 'Кликай']
    b = ['подарки', 'подарочки', 'открытки', 'открыточки', 'стикеры', 'гифки']
    c = ['бесплатно', 'без ОКов', 'на шару']

    text1 = random.choice(a)
    text = '\n\n' + random.choice(b).upper() + '\n' + random.choice(c).upper()

    arrow_pics = ['arrow', 'arrow2']
    arrow_pic = Image.open(
        '/home/deka/Documents/work_scripts/ok_images/arrows/' + random.choice(arrow_pics) + '.png').convert('RGBA')

    for i in range(0, amount):

        # random size of base
        width = 700
        height = 450

        rrr = random.randint(0, 0)

        # border size
        border_width = width + rrr
        border_height = height + rrr

        # random RGB
        r = random.randint(230, 250)
        g = random.randint(70, 90)
        b = random.randint(40, 60)

        # random RGB
        r_outer = random.randint(190, 210)
        g_outer = random.randint(20, 90)
        b_outer = random.randint(20, 55)

        # border
        border = Image.new('RGB', (border_width, border_height), (256, 256, 256))

        inner_color = [r, g, b]  # Color at the center
        outer_color = [r_outer, g_outer, b_outer]  # Color at the corners

        # base image
        base = Image.new('RGB', (width, height), (r, g, b))

        random_number_for_distance_width = round(random.uniform(1.3, 2.5), 2)
        random_number_for_distance_height = round(random.uniform(1.3, 2.5), 2)

        # gradient
        for y in range(height):
            for x in range(width):
                # Find the distance to the center
                distance_to_center = math.sqrt((x - width / random_number_for_distance_width) ** 2 + (y - height / random_number_for_distance_height) ** 2)

                # Make it on a scale from 0 to 1
                distance_to_center = float(distance_to_center) / (math.sqrt(2) * width / random_number_for_distance_width)

                # Calculate r, g, and b values
                r = outer_color[0] * distance_to_center + inner_color[0] * (1 - distance_to_center)
                g = outer_color[1] * distance_to_center + inner_color[1] * (1 - distance_to_center)
                b = outer_color[2] * distance_to_center + inner_color[2] * (1 - distance_to_center)

                # Place the pixel
                base.putpixel((x, y), (int(r), int(g), int(b)))

        font = fonts[2]
        fnt = ImageFont.truetype('/home/deka/Documents/work_scripts/ok_images/fonts/' + font, 70)

        # get a drawing context
        d = ImageDraw.Draw(border)

        border.paste(base, (0, 0))

        d.text((round(width * 0.1), round((height * 0.21))), text1, font=fnt, fill="#fff")

        d.text((round(width * 0.1), round((height * 0.25))), text, font=fnt, fill="#fff")

        rand_height_1 = 20

        gift_pic = Image.open('/home/deka/Documents/work_scripts/ok_images/img_gifts/' +
                              random.choice(number_of_pics)).convert('RGBA').resize((190, 190), Image.ANTIALIAS)

        frame_pic = Image.open(
            '/home/deka/Documents/work_scripts/cards/frame.png').convert(
            'RGBA').resize((360, 360), Image.ANTIALIAS)

        border.paste(frame_pic, (round(width * 0.60 - 90), 30), frame_pic)
        arrow_pic = arrow_pic.resize((60, 60), Image.ANTIALIAS)

        border.paste(arrow_pic, (130, rand_height_1 - 1), arrow_pic)
        border.paste(gift_pic, (round(width*0.60), 110), gift_pic)

        border.save('/home/deka/Documents/work_scripts/cards/output/result/' + str(i + start_index) + '.png', 'PNG', optimize=True, quality=40)


for k in range(0, 600):

    images = []
    for i in os.listdir('/home/deka/Documents/work_scripts/cards/output/result/'):
        file = '/home/deka/Documents/work_scripts/cards/output/result/' + i
        images.append(file)

    # gen images
    img_gen(amount=5, start_index=0)

    # create gif
    create_gif(images[:3], 0.6, str(k))

    # end_time = time.time()
    #
    # result = end_time - start_time
    #
    # print('\n' + str(round(result)) + ' sec')
