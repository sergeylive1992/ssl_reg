import os
import time
import datetime

from selenium import webdriver


def clickpay_get_links():
    path_to_drivers = {'win': os.path.join(os.getcwd(), 'chromedrivers', 'chromedriver.exe'),
                       'linux': os.path.join(os.getcwd(), 'chromedrivers', 'chromedriver')}

    operating_system = 'win'
    print(path_to_drivers[operating_system])
    driver = webdriver.Chrome(path_to_drivers[operating_system])

    file = open('clickpay_' + datetime.datetime.now().strftime('%d-%b-%Y') + '.txt', 'w+')

    # Creds for ClickPay24
    login = 'adv.extland@extland.com'
    password = 'Rj5EdfQnEq'

    # Paths to get links
    paths = ['https://clickpay24.tv/partners/affiliates/', 'https://clickpay24.tv/marketplace/']
    # paths = ['https://clickpay24.tv/marketplace/']

    driver.get('https://clickpay24.tv/login/')

    driver.find_element_by_class_name('left_login').click()
    driver.find_element_by_id('user_name').send_keys(login)
    driver.find_element_by_id('password').send_keys(password)

    time.sleep(1)

    driver.find_element_by_id('userLoginButton').click()

    time.sleep(2)

    for path in paths:
        driver.get(path)
        time.sleep(2)

        pages_number = int(((driver.find_elements_by_class_name('paginationLink'))[-2].get_attribute('href').split('/'))[-1])
        print('Number of pages: ' + str(pages_number))

        offers = []
        for i in range(1, pages_number + 1):
            driver.get(path + str(i))
            time.sleep(2)
            links = driver.find_elements_by_css_selector('.link.link_02')
            for link in links:
                href = link.get_attribute('href')
                if href not in offers:
                    offers.append(href)
                    print(href)
                    file.write(href + '\n')
                else:
                    print('Repeated link: ' + href)

    driver.close()
    driver.quit()
    file.close()


def qwertypay_get_links():
    driver = webdriver.Chrome(os.path.join('chromedrivers', 'chromedriver.exe'))
    driver.implicitly_wait(1)

    file = open('qwertypay_' + datetime.datetime.now().strftime('%d-%b-%Y') + '.txt', 'w+')

    driver.get('http://qwertypay.com/catalog-sort-orders')

    ul = driver.find_element_by_class_name('qwertypay_list')

    last_li = (ul.find_elements_by_tag_name('li')[-1]).find_element_by_tag_name('a').get_attribute('href').split('page=')[-1]
    print(last_li)

    for i in range(1, int(last_li) + 1):
        driver.get('http://qwertypay.com/catalog-sort-orders?&page=' + str(i))
        links = driver.find_elements_by_class_name('rfmsx')
        for link in links:
            print(link.text)
            file.write(link.text + '\n')


    driver.close()
    driver.quit()
    file.close()


def epay_get_links():
    driver = webdriver.Chrome(os.path.join('chromedrivers', 'chromedriver.exe'))

    file = open('epay_' + datetime.datetime.now().strftime('%d-%b-%Y') + '.txt', 'w+')

    driver.get('https://e-pay.club/login/')

    driver.find_element_by_name('login').send_keys('advextland')
    driver.find_element_by_name('passwd').send_keys('Na9DDZr5CaRrdfEsdvsdv')
    driver.find_element_by_xpath('/html/body/div[1]/div[5]/form/button').click()

    time.sleep(1)

    driver.get('https://e-pay.club/marketplace/')

    time.sleep(1)
    for i in range(0, 320):
        try:
            time.sleep(1)
            links = driver.find_elements_by_css_selector('.tovar_hover .name a')
            for link in links:
                print(link.get_attribute('href'))
                file.write(link.get_attribute('href') + '\n')
            driver.find_element_by_xpath('//*[@id="product-container"]/div[11]/a[7]').click()
        except:
            break

    driver.close()
    driver.quit()
    file.close()


clickpay_get_links()
qwertypay_get_links()
epay_get_links()
