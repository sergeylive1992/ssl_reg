import random


domain = 'lleenta.ru'
number = 20000

lines = open('words.txt').readlines()

file = open(domain + '_with_sub_' + str(number) + '.txt', 'w')

for i in range(0, number):

    randomNumber_1 = random.randint(0, 410000)
    randomNumber_2 = random.randint(0, 410000)
    randomNumber_3 = random.randint(0, 410000)
    randomNumber_4 = random.randint(0, 410000)

    line_1 = lines[randomNumber_1]
    line_2 = lines[randomNumber_2]
    line_3 = lines[randomNumber_3]
    line_4 = lines[randomNumber_4]

    words_1 = line_1.split()
    words_2 = line_2.split()
    words_3 = line_3.split()
    words_4 = line_4.split()

    sub_domain = random.choice(words_1)
    word_2 = random.choice(words_2)
    word_3 = random.choice(words_3)
    word_4 = random.choice(words_4)

    symbol = ['_', '-', '/']

    if i % 2 == 0:
        sym = symbol[0]

    elif i % 5:
        sym = symbol[1]

    else:
        sym = symbol[2]

    # link = 'http://' + sub_domain + '.' + domain + '/' + word_2 + '/' + word_3 + sym + word_4
    link = 'http://' + domain + '/news/' + word_2 + '/' + word_3 + sym + word_4
    # link = 'http://' + sub_domain + '.' + domain
    file.write(link + '\n')
    # print(link)
