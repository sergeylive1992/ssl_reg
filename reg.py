import time
import os
import zipfile
import getpass
from selenium import webdriver

user = getpass.getuser()

operating_system = 'win'
path_to_drivers = {'win': os.path.join(os.getcwd(), 'chromedivers', 'chromediver.exe'),
                   'linux': os.path.join(os.getcwd(), 'chromedivers', 'chromediver')}

driver = webdriver.Chrome(path_to_drivers[operating_system])
driver.implicitly_wait(10)

driver2 = webdriver.Chrome(path_to_drivers[operating_system])
driver2.implicitly_wait(10)

domain_to_reg = 'r91flbay9mer'

login = 'fedot-efimov@bk.ru'
password = 'O4hjhgJpY'

path_to_ssl = 'SSL/'
path_to_free_ssl = 'free-ssl/'

link_to_reg = 'https://www.sslforfree.com/create?domains=' + domain_to_reg + '.ru'

# login to ssl-for-free
driver.get('https://www.sslforfree.com/login')
driver.find_element_by_xpath('//*[@id="content_login"]/form/label[1]/input').send_keys(login)
driver.find_element_by_xpath('//*[@id="content_login"]/form/label[2]/input').send_keys(password)
driver.find_element_by_xpath('//*[@id="content_login"]/form/button[2]').click()
print(link_to_reg)
time.sleep(2)

# reg ssl and get data for verification
driver.get(link_to_reg)
driver.find_element_by_xpath('//*[@id="content_create"]/div/div[1]/a[3]').click()
driver.find_element_by_id('content_create_manual_dns_form').click()

# data for verification
host = driver.find_element_by_xpath('//*[@id="content_create_manual_dns_output"]/ol/li[2]/ol/li/strong[1]').text
txt_record = driver.find_element_by_xpath('//*[@id="content_create_manual_dns_output"]/ol/li[2]/ol/li/strong[2]').text

# verify domain by txt record
driver2.get('http://ns1.kiphost.ru/api/?key=qWYPVt6k23c6SrMS&action=form')

driver2.find_element_by_id('domain').send_keys(host)
driver2.find_element_by_xpath('//*[@id="type"]/option[5]').click()
driver2.find_element_by_id('context').send_keys(txt_record)
driver2.find_element_by_id('add').click()

time.sleep(5)

driver.find_element_by_xpath('//*[@id="content_create_manual_dns_output"]/form/button').click()

time.sleep(3)

driver.find_element_by_xpath('//*[@id="certificate_download"]').click()

time.sleep(3)

zip_ref = zipfile.ZipFile('/home/' + user + '/Downloads/sslforfree.zip', 'r')
zip_ref.extractall('/home/' + user + '/Documents/work_scripts/SSL')
zip_ref.close()

zip_file_name = path_to_free_ssl + domain_to_reg + '_ru_crt.zip'
myZipFile = zipfile.ZipFile(zip_file_name, 'w')
myZipFile.write(path_to_ssl + 'ca_bundle.crt', 'ca_1.crt', zipfile.ZIP_DEFLATED)
myZipFile.write(path_to_ssl + 'certificate.crt', domain_to_reg + '_ru.crt', zipfile.ZIP_DEFLATED)
myZipFile.write(path_to_ssl + 'private.key', domain_to_reg + '.ru.key', zipfile.ZIP_DEFLATED)

driver.close()
driver.quit()

driver2.close()
driver2.quit()

os.remove('/home/' + user + '/Downloads/sslforfree.zip')

print('SSL cert was successfully created')


