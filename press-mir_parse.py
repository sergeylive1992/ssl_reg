import time
from selenium import webdriver

driver = webdriver.Chrome()

start = time.time()

driver.implicitly_wait(10)

driver.get('https://press-mir.com')

SCROLL_PAUSE_TIME = 1

# Get scroll height
last_height = driver.execute_script("return document.body.scrollHeight")

# Scroll to get 32 posts
i = 1
while i != 3:
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(SCROLL_PAUSE_TIME)

    # Calculate new scroll height and compare with last scroll height
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        break
    last_height = new_height
    i = i + 1

elements = driver.find_elements_by_css_selector("a[class*='cap']")
elements_img = driver.find_elements_by_css_selector("a[class*='cap']")

count = 1
for element in elements:
    img = element.find_element_by_css_selector('.image > img')
    title = element.find_element_by_css_selector('.title')
    if 'press' in element.get_attribute('href'):
        print(str(count))
        print(title.text)
        print(element.get_attribute('href'))
        print(img.get_attribute('src'))
        count = count + 1

end = time.time()
print('Script executing time: ' + str(end - start))

driver.close()
driver.quit()
