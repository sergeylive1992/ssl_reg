import datetime

from selenium import webdriver
import os
import time


def clickpay_get_links():
    path_to_drivers = {'win': os.path.join(os.getcwd(), 'chromedrivers', 'chromedriver.exe'),
                       'linux': os.path.join(os.getcwd(), 'chromedrivers', 'chromedriver')}

    operating_system = 'win'
    print(path_to_drivers[operating_system])
    driver = webdriver.Chrome(path_to_drivers[operating_system])

    file = open('clickpay_' + datetime.datetime.now().strftime('%d-%b-%Y') + '.txt', 'w+')

    # Creds for ClickPay24
    login = 'adv.extland@extland.com'
    password = 'Rj5EdfQnEq'

    # Paths to get links
    paths = ['https://clickpay24.tv/partners/affiliates/', 'https://clickpay24.tv/marketplace/']
    # paths = ['https://clickpay24.tv/marketplace/']

    driver.get('https://clickpay24.tv/login/')

    driver.find_element_by_class_name('left_login').click()
    driver.find_element_by_id('user_name').send_keys(login)
    driver.find_element_by_id('password').send_keys(password)

    time.sleep(1)

    driver.find_element_by_id('userLoginButton').click()

    time.sleep(2)

    for path in paths:
        driver.get(path)
        time.sleep(2)

        pages_number = int(((driver.find_elements_by_class_name('paginationLink'))[-2].get_attribute('href').split('/'))[-1])
        print('Number of pages: ' + str(pages_number))

        offers = []
        for i in range(1, pages_number + 1):
            driver.get(path + str(i))
            time.sleep(2)
            links = driver.find_elements_by_css_selector('.link.link_02')
            for link in links:
                href = link.get_attribute('href')
                if href not in offers:
                    offers.append(href)
                    print(href)
                    file.write(href + '\n')
                else:
                    print('Repeated link: ' + href)

    driver.close()
    driver.quit()
    file.close()
