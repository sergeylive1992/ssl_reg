import random


def gen_links(domains, number_of_links):
    file = open('links_dms/links_1.txt', 'w')

    for domain in domains:

        lines = open('words_1.txt').readlines()

        for i in range(0, number_of_links):

            words = []
            for k in range(0, 6):
                random_number = random.randint(0, 410000)
                line = lines[random_number]
                word = line.split('\n')[0].lower()
                words.append(word)

            if random_number % 2 == 0:
                link = 'https://ok.ru/dk?cmd=logExternal&st.name=externalLinkRedirect&st.link=https%3A%2F%2Fok.ru%2Fdk%3Fcmd%3DlogExternal%26amp%3Bst.name%3DexternalLinkRedirect%26amp%3Bst.link%3Dhttp%253A%252F%252F' + domain + '%2F' + words[0] + '%2F' + words[1] + '-' + words[2]

            elif random_number % 2 == 3:
                link = 'https://ok.ru/dk?cmd=logExternal&st.name=externalLinkRedirect&st.link=https%3A%2F%2Fok.ru%2Fdk%3Fcmd%3DlogExternal%26amp%3Bst.name%3DexternalLinkRedirect%26amp%3Bst.link%3Dhttp%253A%252F%252F' + domain + '%2F' + words[0] + '%2F' + words[3] + '%2F' + words[4] + '%2F' + words[5]

            else:
                link = 'https://ok.ru/dk?cmd=logExternal&st.name=externalLinkRedirect&st.link=https%3A%2F%2Fok.ru%2Fdk%3Fcmd%3DlogExternal%26amp%3Bst.name%3DexternalLinkRedirect%26amp%3Bst.link%3Dhttp%253A%252F%252F' + domain + '%2F' + words[0] + '-' + words[3] + '_' + words[4] + '%2F' + words[5] + '%2F' + words[2]

            # if random_number % 2 == 0:
            #     link = domain + '/' + words[0] + '/' + words[1] + '-' + words[2]
            #
            # elif random_number % 2 == 3:
            #     link = domain + '/' + words[0] + '%/' + words[3] + '%/' + words[4] + '%/' + words[5]
            #
            # else:
            #     link = domain + '/' + words[0] + '-' + words[3] + '_' + words[4] + '/' + words[5] + '/' + words[2]

            # link = 'http://' + domain + '/' + words[0] + '-okonline/' + words[3] + '-' + str(i + 1)

            file.write(link + '\n')
    file.close()


# lines = open('links_dms/links.txt').readlines()
# random.shuffle(lines)
# open('links_dms/links_mix_3.txt', 'w').writelines(lines)


domains_dms = ['jodosen.ru']

gen_links(domains_dms, 3)