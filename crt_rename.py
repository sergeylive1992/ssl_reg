import zipfile
import getpass
import os

user = getpass.getuser()

domain_to_reg = '21124124124124124124.pro'
path_to_ssl = 'SSL/'
path_to_free_ssl = 'free-ssl/'

zip_file_name = path_to_free_ssl + domain_to_reg.split('.')[0] + '_' + domain_to_reg.split('.')[1] + '_crt.zip'
myZipFile = zipfile.ZipFile(zip_file_name, 'w')
myZipFile.write(path_to_ssl + 'ca_bundle.crt', 'ca_1.crt', zipfile.ZIP_DEFLATED)
myZipFile.write(path_to_ssl + 'certificate.crt', domain_to_reg.split('.')[0] + '_' + domain_to_reg.split('.')[1] + '.crt', zipfile.ZIP_DEFLATED)
myZipFile.write(path_to_ssl + 'private.key', domain_to_reg.split('.')[0] + '.' + domain_to_reg.split('.')[1] + '.key', zipfile.ZIP_DEFLATED)

os.remove('/home/' + user + '/Downloads/sslforfree.zip')