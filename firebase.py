import os
from selenium import webdriver

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")

driver = webdriver.Chrome(os.getcwd() + '/' + 'chromedriver', chrome_options=chrome_options)

driver.implicitly_wait(10)

driver.get('https://accounts.google.com/ServiceLogin')

driver.find_element_by_xpath('//*[@id="identifierId"]').send_keys('')