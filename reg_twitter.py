import time
from selenium import webdriver

driver = webdriver.Chrome()
driver.implicitly_wait(10)

# Creds
login = '+79017595818'
password = 'A3lusQcyoW'

# FILES_LOCATION = "C:\\Users\\deka\Desktop\\twitter_links\\"

# original links location
file = open("tumblrs.txt", "r+")

# generated links location
file_2_name = "twitter.txt"

lines = file.readlines()

count = 1
driver.get('https://twitter.com/login')
# driver.find_element_by_xpath('//*[@id="doc"]/div[1]/div/div[1]/div[2]/a[3]').click()
driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[1]/input').send_keys(login)
driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[2]/input').send_keys(password)
driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/div[2]/button').click()

if driver.title == 'Verify your identity':
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="challenge_response"]').send_keys(login)
    driver.find_element_by_xpath('//*[@id="email_challenge_submit"]').click()

username = driver.find_element_by_xpath('//*[@id="page-container"]/div[1]/div[1]/div/div[2]/span/a/span/b').text
print("Username: " + username)

try:
    for i in range(0,150):

        tumblr = lines[i]
        driver.find_element_by_xpath('//*[@id="tweet-box-home-timeline"]').send_keys(tumblr)
        driver.find_element_by_xpath('//*[@id="timeline"]/div[2]/div/form/div[3]/div[2]/button').click()
        time.sleep(2)
        print(str(i) + " " + tumblr)

except:
    print("Error: " + str(ReferenceError))
    file.close()
    driver.close()
    driver.quit()

driver.get('https://twitter.com/' + username)

SCROLL_PAUSE_TIME = 0.5

# Get scroll height
last_height = driver.execute_script("return document.body.scrollHeight")

while True:
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(SCROLL_PAUSE_TIME)

    # Calculate new scroll height and compare with last scroll height
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        break
    last_height = new_height

links = driver.find_elements_by_css_selector('a[title*="tumblr.com"]')

# file_2 = open(file_2_name, "r+")
count_2 = 1
for k in links:
    t_link = k.get_attribute('href')
    print(str(count_2) + " " + k.get_attribute('href'))
    count_2 = count_2 + 1
    # file_2.write(k.get_attribute('href') + '\n')

file.close()
# file_2.close()
driver.close()
driver.quit()
