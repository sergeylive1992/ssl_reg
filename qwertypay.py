import datetime
import os

from selenium import webdriver


def qwertypay_get_links():
    driver = webdriver.Chrome(os.path.join('chromedrivers', 'chromedriver.exe'))
    driver.implicitly_wait(1)

    file = open('qwertypay_' + datetime.datetime.now().strftime('%d-%b-%Y') + '.txt', 'w+')

    driver.get('http://qwertypay.com/catalog-sort-orders')

    ul = driver.find_element_by_class_name('qwertypay_list')

    last_li = (ul.find_elements_by_tag_name('li')[-1]).find_element_by_tag_name('a').get_attribute('href').split('page=')[-1]
    print(last_li)

    for i in range(1, int(last_li) + 1):
        driver.get('http://qwertypay.com/catalog-sort-orders?&page=' + str(i))
        links = driver.find_elements_by_class_name('rfmsx')
        for link in links:
            print(link.text)


    driver.close()
    driver.quit()
    file.close()